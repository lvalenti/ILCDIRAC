"""Module to run job tests with less in-direction."""

from __future__ import print_function

import glob
import os
import tempfile
import shutil
import pwd

import pytest


from Tests.Utilities.GeneralUtils import running_on_docker


TEST_FILE_DIR = 'Tests/Files'


@pytest.fixture(scope="module")
def diracMagicParseCommandLine():
  """Do the magic command line parsing once for the module."""
  print("Doing the magic")
  from DIRAC import gLogger
  gLogger.setLevel("DEBUG")
  from DIRAC.Core.Base import Script
  Script.parseCommandLine()

  uid = os.getuid()
  user_info = pwd.getpwuid(uid)
  homedir = os.path.join(os.sep + 'home', user_info.pw_name)
  cvmfstestsdir = 'cvmfstests'

  if running_on_docker():
    localsitelocalarea = os.path.join(os.getcwd(), cvmfstestsdir)
  else:
    localsitelocalarea = os.path.join(homedir, cvmfstestsdir)
  from DIRAC import gConfig
  gConfig.setOptionValue('/LocalSite/LocalArea', localsitelocalarea)
  gConfig.setOptionValue('/LocalSite/LocalSE', "CERN-DIP-4")
  gConfig.setOptionValue('/Resources/Countries/local/AssignedTo', 'ch')


def assertOutputFileExists(outputFile):
  """Check if outputfile exists in the random LOCAL_... folder."""
  files = glob.glob(os.path.join('Local_*', outputFile))
  assert len(files) == 1, 'outputfile %s not found' % outputFile


@pytest.fixture
def getJob():
  """Define a generic job, it should be always the same."""
  from ILCDIRAC.Interfaces.API.NewInterface.UserJob import UserJob
  myjob = UserJob()
  myjob.setName("Testing")
  myjob.setJobGroup("Tests")
  myjob.setCPUTime(30000)
  myjob.dontPromptMe()
  myjob.setLogLevel("VERBOSE")
  myjob.setPlatform("x86_64-slc5-gcc43-opt")
  myjob.setOutputSandbox(["*.log", "*.xml", "*.sh"])
  myjob._addParameter(myjob.workflow, 'TestFailover', 'String', True, 'Test failoverRequest')
  myjob._addParameter(myjob.workflow, 'Platform', 'JDL', "x86_64-slc5-gcc43-opt", 'OS Platform')
  # if self.ildConfig:
  #   myjob.setILDConfig(self.ildConfig)
  return myjob


@pytest.fixture
def jobdir():
  """Create a temporary job directory."""
  print("I will run the tests locally.")
  from DIRAC import gConfig
  localarea = gConfig.getValue("/LocalSite/LocalArea", "")
  if not localarea:
    assert False, "You need to have /LocalSite/LocalArea defined in your dirac.cfg"
  if localarea.find("/afs") == 0:
    print("check ${HOME}/.dirac.cfg and ${DIRAC}/etc/dirac.cfg")
    assert False, "Don't set /LocalSite/LocalArea set to /afs/... as you'll get to install there"
  print("To run locally, I will create a temp directory here.")
  curdir = os.getcwd()
  tmpdir = tempfile.mkdtemp("", dir="./")
  os.chdir(tmpdir)
  yield (tmpdir, curdir)
  os.chdir(curdir)
  if 'CI_JOB_ID' not in os.environ:
    shutil.rmtree(tmpdir)


@pytest.mark.integration
def test_gaudiapp(getJob, jobdir, diracMagicParseCommandLine):
  """Run a job with the gaudiapp application."""
  _tmpdir, curdir = jobdir
  steeringFile = 'gaudiapp_test_steering.py'
  shutil.copy(os.path.join(curdir, TEST_FILE_DIR, steeringFile), os.getcwd())

  from ILCDIRAC.Interfaces.API.DiracILC import DiracILC
  from ILCDIRAC.Interfaces.API.NewInterface.Applications import GaudiApp

  dIlc = DiracILC()

  job = getJob
  job.setName("GaudiTest")
  outputFile = "output.root"
  job.setOutputData(outputFile, OutputPath="gaudiTest")

  ga = GaudiApp()
  ga.setVersion('key4hep_nightly')
  ga.setExecutable('k4run')
  ga.setNumberOfEvents(3)
  ga.setSteeringFile(steeringFile)
  ga.setDetectorModel("DetFCCeeIDEA-LAr")
  ga.setArguments("--GenAlg.MomentumRangeParticleGun.MomentumMin 1000 "
                  "--GenAlg.MomentumRangeParticleGun.MomentumMax 1000 "
                  "--GenAlg.MomentumRangeParticleGun.PdgCodes 11")

  # ga.setOutputFile("output.root", outputFlag = "--PrependThisFlag.filename")
  ga.setOutputFile("output.root")

  res = job.append(ga)
  if not res['OK']:
    print(res['Message'])
    assert False, "Failed to setup gaudiapp"

  result = job.submit(dIlc, mode='local')
  assert result['OK'], "Failed to run job: " + result.get('Message')
  assertOutputFileExists(outputFile)


@pytest.mark.integration
def test_unicodeoutput(getJob, jobdir, diracMagicParseCommandLine):
  """Run a job with an application that prints out unicode characters."""
  _tmpdir, curdir = jobdir
  steeringFile = 'unicodesandwich.txt'
  shutil.copy(os.path.join(curdir, TEST_FILE_DIR, steeringFile), os.getcwd())

  from ILCDIRAC.Interfaces.API.DiracILC import DiracILC
  from ILCDIRAC.Interfaces.API.NewInterface.Applications import GenericApplication

  dIlc = DiracILC()

  job = getJob
  job.setName("UniCodeTest")
  job.setInputSandbox([steeringFile])

  ga = GenericApplication()
  ga.setSetupScript('/dev/null')
  ga.setScript('/usr/bin/cat')
  ga.setArguments(steeringFile)
  res = job.append(ga)
  if not res['OK']:
    print(res['Message'])
    assert False, "Failed to setup genericapp"

  ga = GenericApplication()
  ga.setSetupScript('/dev/null')
  ga.setScript('/usr/bin/cat')
  outputFile = "output.txt"
  ga.setArguments(steeringFile + " > " + outputFile)
  res = job.append(ga)
  if not res['OK']:
    print(res['Message'])
    assert False, "Failed to setup genericapp"

  result = job.submit(dIlc, mode='local')
  assert result['OK'], "Failed to run job: " + result.get('Message')
  assertOutputFileExists(outputFile)
