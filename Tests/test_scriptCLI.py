"""Test the command line options for the scrips."""
from copy import deepcopy
import importlib
import logging
import random
import pytest


def ris():
  """Return random int as string."""
  return str(random.randint(0, 100000))


def fis():
  """Return foo plus random int as string."""
  return 'foo' + str(random.randint(0, 100000))


SCRIPTS = [('ILCDIRAC.Core.scripts.dirac_ilc_add_cvmfs_software', {}),
           ('ILCDIRAC.Core.scripts.dirac_ilc_add_software', {}),
           ('ILCDIRAC.Core.scripts.dirac_ilc_add_user', {'Email': {'S': ['foo@bar.baz'], 'E': [fis()]},
                                                         'VO': {'S': ['calice'], 'E': ['vo']}}),
           ('ILCDIRAC.Core.scripts.dirac_ilc_add_whizard', {}),
           ('ILCDIRAC.Core.scripts.dirac_ilc_list_users', {'VO': {'S': ['calice'], 'E': ['vo']}}),
           ('ILCDIRAC.ILCTransformationSystem.scripts.dirac_clic_make_productions', {'configFile': {'E': ['any'],
                                                                                                    'S': []}}),
           ('ILCDIRAC.ILCTransformationSystem.scripts.dirac_ilc_add_tasks_to_prod', {'Tasks':
                                                                                     {'E': [fis()], 'S': [ris()]},
                                                                                     'ProductionID':
                                                                                     {'E': [fis()], 'S': [ris()]}}),
           ('ILCDIRAC.ILCTransformationSystem.scripts.dirac_ilc_filestatus_transformation', {}),
           ('ILCDIRAC.ILCTransformationSystem.scripts.dirac_ilc_get_info', {'ProductionID':
                                                                            {'E': [fis()], 'S': [ris()]}}),
           ('ILCDIRAC.ILCTransformationSystem.scripts.dirac_ilc_get_prod_log',
            {'Query': {'S': ['Foo:Bar'], 'E': ['FooBar']}}),
           #  ('ILCDIRAC.ILCTransformationSystem.scripts.dirac_ilc_make_ddsimtarball', {}),
           ('ILCDIRAC.ILCTransformationSystem.scripts.dirac_ilc_moving_transformation', {'ps': True}),
           ('ILCDIRAC.ILCTransformationSystem.scripts.dirac_ilc_production_summary', {'prods':
                                                                                      {'E': [fis()], 'S': [ris()]},
                                                                                      'sample_size':
                                                                                      {'E': [fis()], 'S': [ris()]}}),
           ('ILCDIRAC.ILCTransformationSystem.scripts.dirac_ilc_replication_transformation', {'ps': True}),
           ('ILCDIRAC.ILCTransformationSystem.scripts.dirac_ilc_upload_gen_files', {'Energy':
                                                                                    {'E': [fis()], 'S': [ris()]},
                                                                                    'EvtID':
                                                                                    {'E': [fis()], 'S': [ris()]},
                                                                                    'NumberOfEvents':
                                                                                    {'E': [fis()], 'S': [ris()]},
                                                                                    'XSectionError':
                                                                                    {'E': [fis()], 'S': [ris()]},
                                                                                    'XSection':
                                                                                    {'E': [fis()], 'S': [ris()]},
                                                                                    'Luminosity':
                                                                                    {'E': [fis()], 'S': [ris()]}}),
           ('ILCDIRAC.Interfaces.scripts.dirac_ilc_find_in_FC', {}),
           ('ILCDIRAC.Interfaces.scripts.dirac_ilc_show_software', {}),
           ('ILCDIRAC.Interfaces.scripts.dirac_repo_create_lfn_list', {}),
           ('ILCDIRAC.Interfaces.scripts.dirac_repo_retrieve_jobs_output_data', {}),
           ('ILCDIRAC.Interfaces.scripts.dirac_repo_retrieve_jobs_output', {}),
           ('ILCDIRAC.Interfaces.scripts.TestAndProbeSites', {}),
           ]


@pytest.mark.parametrize('scriptPath, options', SCRIPTS)
def test_scriptCLI(caplog, scriptPath, options, mocker):
  """Check that calling the callback does something."""
  caplog.set_level(logging.DEBUG)
  rsm = mocker.Mock()
  mocker.patch('DIRAC.Core.Base.Script.registerSwitch', new=rsm)
  try:
    module = importlib.import_module(scriptPath)
  except ImportError:
    assert False
  try:
    paramClass = getattr(module, '_Params')
    theParams = paramClass()
    if options.get('ps'):
      theParams.registerSwitches(rsm)
    else:
      theParams.registerSwitches()
  except AttributeError as e:
    logging.info(dir(module))
    assert False, 'ERROR: ' + str(e)
    return theParams

  for call in rsm.call_args_list:
    logging.info(call)
    flag = call[0][1].strip('=:')
    callback = call[0][3]
    if isinstance(callback, bool):
      continue
    paramBefor = deepcopy(vars(theParams))
    errorFlags = options.get(flag, {}).get('E', [])
    successFlags = options.get(flag, {}).get('S', [ris(), fis()])
    for value in errorFlags:
      retVal = callback(value)
      assert not retVal['OK'], 'Value should cause failure'

    for value in successFlags:
      retVal = callback(value)
      if not retVal['OK']:
        assert False, 'failure for ' + value + flag + str(successFlags) + str(options)
        continue

      paramAfter = vars(theParams)
      logging.info('befor: %s', sorted(paramBefor))
      logging.info('after: %s', sorted(paramAfter))
      logging.info('befor: %s', paramBefor.values())
      logging.info('after: %s', paramAfter.values())
      assert paramBefor != paramAfter
      # assert any(randVal in aVal for aVal in paramAfter.values() if aVal)
      diff = 0
      for name, befor in paramBefor.items():
        after = paramAfter[name]
        logging.info('Befor: %r -- After: %r', befor, after)
        if after == value:
          logging.info('The Rand Val: %r', value)
          diff = 1
          break
        if befor != after:
          logging.info('Difference')
          diff += 1
      assert diff == 1
