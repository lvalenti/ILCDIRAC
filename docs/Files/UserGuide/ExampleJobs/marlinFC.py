from DIRAC import gLogger
from DIRAC.Core.Base import Script
Script.parseCommandLine()

from DIRAC.Resources.Catalog.FileCatalog import FileCatalog
from ILCDIRAC.Interfaces.API.DiracILC import DiracILC
from ILCDIRAC.Interfaces.API.NewInterface.UserJob import UserJob
from ILCDIRAC.Interfaces.API.NewInterface.Applications import Marlin

dIlc = DiracILC()

inputData = FileCatalog().findFilesByMetadata({'ProdID': 9275, 'DataType': 'SIM'})
if not inputData['OK']:
  gLogger.error('ERROR: Failed to find inputData', inputData['Message'])
  exit(1)

gLogger.notice('Number of Input Data files:', len(inputData['Value']))
# truncate to 10 files
inputData = inputData['Value'][:10]

job = UserJob()
job.setName("SplitTest_%n")  # %n will be replaced by the task number
job.setOutputSandbox("*.log")
job.setOutputData("RecoTest.slcio", OutputPath="RecoTest2")
job.setSplitInputData(inputData, numberOfFilesPerJob=1)
job.setCLICConfig('ILCSoft-2020-02-07')

marl = Marlin()
marl.setVersion('ILCSoft-2020-02-07_gcc62')
marl.setSteeringFile('clicReconstruction.xml')
marl.setOutputFile('RecoTest.slcio')

res = job.append(marl)
if not res['OK']:
  gLogger.error(res['Message'])
  exit(1)

job.submit(dIlc)
