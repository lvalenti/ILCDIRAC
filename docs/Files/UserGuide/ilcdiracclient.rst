Setting up an iLCDirac environment
==================================

.. set highlighting to console input/output
.. highlight:: console

.. warning::

   The DIRAC environment is usually incompatible with other environments, and
   therefore requires a dedicated terminal/shell. Do not add the iLCDirac ``bashrc``
   file to your default shell environment. Only source the iLCDirac setup scrip
   when you need it


.. contents::


.. _client:

Client Python Version
---------------------

Both the Python2 and the Python3 clients are compatible with the same server
installations, and will remain compatible also when the Servers are up-dated to use a
Python 3 based installation. This is the case until iLCDirac will be based on DIRAC
version 8, when only Python 3 clients will be supported. Therefore please try out the
Python3 client and report any issues.


Using a pre existing installation (e.g. at CERN)
------------------------------------------------

If you rely on someone's installation (or your own) you have normally access to
the directory in which iLCDirac has been installed. In that directory there is a
setup ``bashrc``, that needs sourcing to get
the right environment. For example when CVMFS is available you can run

.. tabs::

    .. code-tab:: console Python 3

	source /cvmfs/clicdp.cern.ch/DIRACPy3/bashrc

    .. code-tab:: console Python 2

	source /cvmfs/clicdp.cern.ch/DIRAC/bashrc

Once this file has been sourced, you get access to all the DIRAC and iLCDirac
commands, as well as the python API. You can proceed to the `Job section <submittingjobs>`_.



Getting a Proxy
```````````````

.. note ::

  See :ref:`convCert` for creating the certificate files DIRAC
  requires.


Once you have sourced the DIRAC environment file, you can run::

  dirac-proxy-init -g ilc_user

or::

  dirac-proxy-init -g calice_user

depending on the **VO** you want a proxy for. ``ilc_user``, ``calice_user``, and ``fcc_user`` are
the default user groups for the respective VOs. The two VOs have access to different resources, it's
sometimes needed to get the right proxy (in particular for data access). Also, this has an effect on
the grid sites on which one can run, for instance, only the DESY-HH and IPNL sites are available to
the CALICE users (in ILCDIRAC, and for the moment).



Installing iLCDirac
-------------------

When not having access to a pre installed DIRAC, you need to install it
yourself. The procedure is as follows:

.. tabs::

   .. tab:: Python 3


      .. code-block:: console

	 curl -LO https://github.com/DIRACGrid/DIRACOS2/releases/latest/download/DIRACOS-Linux-$(uname -m).sh
	 bash DIRACOS-Linux-$(uname -m).sh
	 rm DIRACOS-Linux-$(uname -m).sh
	 source diracos/diracosrc
	 pip install ILCDIRAC

      This also creates the ``diracos/diracosrc`` file needed to get the DIRAC
      environment that you obtain with:

      .. code-block:: console

	 source diracos/diracosrc

   .. tab:: Python 2

      .. code-block:: console

	 wget -O dirac-install -np https://raw.githubusercontent.com/DIRACGrid/management/master/dirac-install.py --no-check-certificate
	 chmod +x dirac-install
	 ./dirac-install -V ILCDIRAC


      This also creates the ``bashrc``  file needed to get the DIRAC
      environment that you obtain with::

	 source bashrc

After sourcing the respective rc file you need to configure ILCDIRAC to make the client know where
to get the services from. This requires the presence of a valid "proxy". First, if you don't have
the ``pem`` files, you can get them with::

   dirac-cert-convert.sh grid.p12

where ``grid.p12`` is the file you got from the web browser export. If you
already have the ``pem`` files, this can be skipped.

To get a valid proxy, run::

   dirac-proxy-init -x -N

.. warning::

   This is not a DIRAC proxy. A DIRAC proxy is obtained by omitting the ``-x``. Do
   not use ``-x`` for anything but installing DIRAC. See below how to obtain a valid
   proxy to use the system.

Then run the configuration::

   dirac-configure -S ILC-Production -C dips://voilcdiracconfig.cern.ch:9135/Configuration/Server \
     --SkipCAChecks

In principle, if this commands runs fine, you should be able to got to the next section.

Don't forget to source the ``bashrc`` / ``diracosrc`` file whenever using DIRAC.


Updating iLCDirac
`````````````````

To update to version v31r0, please install a completely new iLCDirac version.  The ``bashrc`` file
is otherwise not updated, and your client would not be functional.

The Python3 installation can be updated with ``pip``.


.. _caAndCRLs:

Certification Authorities and Certificate Revocation Lists
``````````````````````````````````````````````````````````

If you are installing your own iLCDirac client, you have to keep the
Certification Authorities (CAs) and Certificate Revocation Lists (CRLs)
up-to-date. If your system installs and updates these files automatically you
don't have to do anything. See if the folder ``/etc/grid-security/certificates``
exists.

If you don't have this folder, you need to occasionally update the files
yourself. They will be located in the ``$DIRAC/etc/grid-security/certificates``
folder in this case.

Use the :doc:`AdministratorGuide/CommandReference/dirac-admin-get-CAs` command to update the files.

A clear sign of when to run the above command is an error message during the
call to :doc:`UserGuide/CommandReference/Others/dirac-proxy-init` about CRLs being out-of-date.


.. tabs::

   .. tab:: Python 2

       In this case, add the following line to ``$DIRAC/bashrc``::

	 export X509_CERT_DIR=$DIRAC/etc/grid-security/certificates

       Then source the ``bashrc`` file again.
